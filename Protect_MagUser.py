#!/usr/bin/python3

import sys
import os
import requests
import json
import subprocess
import urllib3
import base64
import syslog

#####################################################
#
#  Class:  Protect_MagUser
#
#  Provides methods to manage a MAG user session
#
#####################################################
class Protect_MagUser:
    magUser = ""
    magPassword = ""
    magIP = ""

    authToken = ""
    authTCAToken = ""
    authCookie = ""
    authSession = ""

    def getToken(self):
        return(self.authToken)

    def getUser(self):
        return(self.magUser)

    def getTCAToken(self):
        return(self.authTCAToken)

    def getCookie(self):
        return(self.authCookie)

    def getSession(self):
        return(self.authSession)

    def getMagIP(self):
        return(self.magIP)

    def getCredentials(self):
        return self.authCookie, self.authToken, self.magIP, self.authSession

    #############################################
    #
    #  Constructor
    #
    #############################################
    def __init__(self, user, password):

        self.LOG_TAG=self.__class__.__name__
        self.magUser = user
        self.magPassword = password

    #############################################
    #
    #  Method:  login
    #
    #  Returns: REST response code 
    #
    #  Return of 200 indicates success
    #
    #############################################
    def login(self):

        responseText = ""

        ##  Get the MAG IP

        self.magIP = json.loads(subprocess.check_output("kubectl get svc mag-telegraf -o json", shell=True))['spec']['clusterIP']

        ##  Login the user

        authURL = 'https://'+self.magIP+'/login/api/system/sessions/'
        authRequest = requests.post(authURL, auth=(self.magUser, self.magPassword), verify=False)
        authResponseCode = authRequest.status_code
        if authResponseCode == 200:

            ## MAG authentication. Save token and cookie for future API calls

            self.authToken = {'Authorization' : authRequest.headers['Authorization']}
            self.authCookie = {'vigil_auth' : authRequest.headers['Set-Cookie'].split('vigil_auth=')[-1]}
            self.authSession = json.loads(authRequest.text)['id']

            #  Login to TCA also in case we need to do anything with reports
            
            tcaUser={"username":self.magUser}
            headers = self.authToken
            headers.update({'Content-Type':'application/json'})
            authURL = 'https://'+self.magIP+'/tcaapi/rest/v1/login/'

            r = requests.post(authURL, headers=headers, cookies=self.authCookie, data=json.dumps(tcaUser), verify=False)
            if r.status_code == 200:
                self.authTCAToken = r.json()['token']
            else:
                responseText =  json.dumps(authRequest.text)
        else:
            responseText =  json.dumps(authRequest.text)

        return authResponseCode, responseText

    #############################################
    #
    #  Method:  logout
    #
    #  Returns:  0 if successful, -1 if failed
    #############################################
    def logout(self):

        clearURL = 'https://'+self.magIP+'/api/system/sessions/'+str(self.authSession)+'?inactive=false'
        clearRequest = requests.delete(clearURL, cookies=self.authCookie, headers=self.authToken, verify=False)

        return 0 if clearRequest.status_code == 200 else -1

