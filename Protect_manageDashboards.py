#!/usr/bin/env python3

###################################################################################
#
#   Protect_manageDashboards.py
#
#   Manage Dashboards
#     1.  List dashboards
#     2.  Execute dashboard report
#
###################################################################################

import argparse
import requests
import json
import subprocess
import urllib3
import base64
import syslog
import os
import sys
from pathlib import Path
from Protect_MagUser import Protect_MagUser
import pdb 

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

pgmName = Path(sys.argv[0]).stem

########################################################
#
#  Method: listDashboardReports
#
########################################################
def listDashboardReports(reports):
    print("\nDashboard reports:\n")
    index=1
    for d in reports:
        print("{}. NAME: {}, ID: {}, TYPE: {}, DESCRIPTION: {},  STATUS: {}".format(index, d['name'], d['id'], d['type'], d['description'] if len(d['description']) else 'None', 'enabled' if d['enabled'] == 'true' else 'disabled'))
        index +=1

########################################################
#
#  Method: main
#
########################################################
def main():

    pgmName = Path(sys.argv[0]).stem

    #  Setup to parse the command line arguments

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    required= parser.add_argument_group(title='Required arguments')
    optional= parser.add_argument_group(title='Optional arguments')

    optional.add_argument('-d',  action='store_true', default=False, dest='bDebug', help='Enable debug logging (optional)')
    optional.add_argument('-filter',  action='store', default='', dest='devicesFilter', 
                          help=
                          "Text file that contains list of devices to be updated, one device per line."
                          "If no filter file is specified, all devices in the devicesFile will be updated."
                          )
    optional.add_argument('-i',  action='store', dest='id', help='Report ID, needed to run report)')

    required.add_argument('-u',  action='store', dest='maguser', required=True, help='MAG user ID')
    required.add_argument('-p',  action='store', dest='magpassword', required=True, help='MAG user password')
    required.add_argument('-a',  action='store', dest='action', required=True, choices=['list', 'run'], help='Action to be performed (list, execute)')

    #  Print help if no arguments

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    #  Parse the arguments

    args = parser.parse_args()    #  Get the arguments

    if args.bDebug == True:
        pdb.set_trace()

    ##  Ensure that the ID is passed for the 'run' action

    if args.action == 'run'  and  args.id is None:
        print("The -i argument is required for the 'run' action")
        parser.print_help(sys.stderr)
        sys.exit(1)

    ##  Instantiate the Mag object and Login

    magUser = Protect_MagUser(args.maguser, args.magpassword)

    status, errMsg = magUser.login()
    if status != 200:
        print("{}: Failed to login as user {}, error={}".format(pgmName, args.maguser, status))
        sys.exit(1)

    ##  Get the list of dashboard reports

    dashboardReports = []
    tcaUser={"username":magUser.getUser()}
    address = "https://" + magUser.getMagIP() + "/tcaapi/rest/v1/jobs/"
    headers = {'Tca-Session-Token':magUser.getTCAToken()}
    r = requests.get(address, headers=headers, cookies=magUser.getCookie(), verify=False)
    if r.status_code == 200:
        dashboardReports = r.json()['jobs']
    else:
        print("Failed to retrieve dashboard reports, status is {}".format(r.status_code))

    ##  If listing the dashboards ....
    if args.action == 'list':
        listDashboardReports(dashboardReports)

    ##  Execute the dashboard report

    elif args.action == 'run':
        tcaUser={"username":args.maguser}
        address = "https://" + magUser.getMagIP() + "/tcaapi/rest/v1/control/job/run/"+args.id
        headers = {'Tca-Session-Token':magUser.getTCAToken()}
        idx = next((index for (index, d) in enumerate(dashboardReports) if d["id"] == args.id), None)
        if idx is None:
            print("Report ID {} does not exist.".format(args.id))
        else:
            r = requests.post(address, headers=headers, cookies=magUser.getCookie(), verify=False)
            if r.status_code == 200:
                print("Successfully ran the report for '{}' (ID={})".format(dashboardReports[idx]['name'], dashboardReports[idx]['id']))
            else:
                print("Failed to run report {} (ID={})".format(dashboardReports[idx['name']], dashboardReports[idx['id']]))
        
    magUser.logout()    
    
    sys.exit(0)

if __name__ == '__main__':
    main()

